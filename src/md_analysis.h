//
// Created by s1205916 on 18/06/15.
//
#pragma once

#include "types.h"
#include <unordered_map>

namespace mol_dyn {
    namespace analysis {
        coord perVec(const coord &cm1, const coord &cm2, const crystal &c, coord &index);

        double perDist(const coord &c1, const coord &c2, const crystal &crs, coord &index);

        void identLayers(std::vector<mol_dyn::crystal> &md, int noLayers);

        int IDMol(const molec &m, const int no_atoms);

        molCol identMol(crystal &c, const int no_atoms);

        std::vector<molCol> identMolCol(std::vector<crystal> &md, const MolColMethod method);

        timeCol extractVel(const std::vector<molCol> &mCol, const std::vector<crystal> &crs);

        std::vector<freqPt> DFT(const std::vector<timePt> &vec, double T);

        double XRD(const crystal &c, int k, int l, int m);

        freqPt interp(const freqPt &inf, const freqPt &sup, double fr);

        std::vector<freqPt> combineMaxRes(const std::vector<std::vector<freqPt> > &vecCol);

        std::vector<freqPt> combineMinRes(const std::vector<std::vector<freqPt> > &vecCol);

        std::vector<freqPt> DFTCol(const std::vector<timeCol> &vecCol, double prag);

        crystal avgPos(const std::vector<crystal> &col);

        std::vector<freqPt> smooth(std::vector<freqPt> &data, int a);

        std::vector<double> thetaDeviationsInRad(const mol_dyn::molec_positions&);

        std::vector<double> thetaDeviationsInRad(const mol_dyn::molec_positions& mp, const mol_dyn::coord& c);

        std::vector<double> thetaDeviationsInRad_abs(const mol_dyn::molec_positions&);

        std::unordered_map<int, mol_dyn::molec_positions> sortByMolecule(const std::vector<mol_dyn::molCol>&);

        Image_combined boundaries_crossed(const molec& molecule, const crystal& c);

        mol_dyn::molecules_snapshot get_molecule_states(const molCol& mCol, const crystal&);

        void identLayers(crystal &crystal, int noLayers);

        void quadrupole_sum(const std::vector<molec> &molecules, const mol_dyn::crystal &c, double* sum_value,
                            double nearest_neighbour_dist);

    }
}
