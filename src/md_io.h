//
// Created by s1205916 on 18/06/15.
//

#ifndef MD_IO_H
#define MD_IO_H
#include "types.h"

namespace mol_dyn {
    namespace IO {
        mol_dyn::crystal loadCell(std::ifstream &in);
        std::vector<mol_dyn::crystal> loadMD(std::ifstream &in, int noAtoms, int ignore, int noIter);

        void createMOL(std::ofstream &out, std::vector<mol_dyn::crystal> &col);

        void createXYZ(std::ofstream &out, mol_dyn::crystal &col);

        std::ostream &write_molecular_bonds(std::ostream &, const std::vector<molec> &, const mol_dyn::crystal &);

        //Template function can not be split into definition and declaration.
        template<typename T>
        std::ostream &write_vector(std::ostream &ostr, const std::vector<T> &to_write, std::string delimiter = "\n") {
            for (typename std::vector<T>::size_type i = 0; i < to_write.size(); ++i) {
                ostr << to_write[i] << delimiter;
            }
            ostr << std::endl;
            return ostr;
        }



    }
}



#endif //MD_IO_H
