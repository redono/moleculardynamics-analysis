//
// Created by s1205916 on 30/06/15.
//

#include <iostream>
#include <algorithm>
#include <fstream>
#include <cmath>
#include "types.h"
#include "md_io.h"
#include "md_math.h"
#include "md_analysis.h"

using namespace mol_dyn;
using std::ifstream;
using std::ofstream;
using std::string;
using std::vector;
using std::cout;
using std::endl;
using std::cerr;

int main(int argc, char** argv)
{

    if(argc != 3){
        cerr << "Usage: angularvariation <seedname>.md <no. atoms>" << endl;
        exit(EXIT_FAILURE);
    }

    string filename(argv[1]);
    int no_atoms = atoi(argv[2]);
    ifstream ifstr(filename);
    if(!ifstr){
        cerr << "Could not open file. " << endl << "Error code: " << ifstr.rdstate() << endl;
        cerr << "Usage: angularvariation <seedname>.md <no. atoms> <no. layers>" << endl;
        exit(EXIT_FAILURE);
    }

    vector<crystal> md = IO::loadMD(ifstr, no_atoms, 0,0);
    //analysis::identLayers(md, no_layers);
    math::convertUnits(md);
    vector<molCol> mCol = analysis::identMolCol(md, MolColMethod::CONST_SET);

    auto by_molec_id = analysis::sortByMolecule(mCol);
    for(auto it = by_molec_id.cbegin(); it != by_molec_id.cend(); ++it){
        for(auto i = 0; i < it->second.positions.size(); ++i){
            std::cout << math::cart_to_spherical(math::vec_orientation(it->second.positions[i])) << endl;
        }
    }
    return 0;
}

