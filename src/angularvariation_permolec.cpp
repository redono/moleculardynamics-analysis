#include <iostream>
#include <algorithm>
#include <fstream>
#include <cmath>
#include "types.h"
#include "md_io.h"
#include "md_math.h"
#include "md_analysis.h"

using namespace mol_dyn;
using std::ifstream;
using std::ofstream;
using std::string;
using std::vector;
using std::cout;
using std::endl;
using std::cerr;

int main(int argc, char** argv)
{

    if(argc != 3){
        cerr << "Usage: angularvariation <seedname>.md <no. atoms> <no. layers>" << endl;
        exit(EXIT_FAILURE);
    }
    string filename(argv[1]);
    int no_atoms = atoi(argv[2]);
    ifstream ifstr(filename);
    if(!ifstr){
        cerr << "Could not open file. " << endl << "Error code: " << ifstr.rdstate() << endl;
        cerr << "Usage: angularvariation <seedname>.md <no. atoms> <no. layers>" << endl;
        exit(EXIT_FAILURE);
    }
    vector<crystal> md = IO::loadMD(ifstr, no_atoms, 0,0);
    math::convertUnits(md);
    vector<molCol> mCol = analysis::identMolCol(md, MolColMethod::UPDATE_SET);
    auto by_molec_id = analysis::sortByMolecule(mCol);
    coord zaxis(0,0,1);
    vector<vector<double>> molec_angles;
    for (auto it = by_molec_id.cbegin(); it != by_molec_id.cend(); ++it){
        molec_angles.push_back(analysis::angleDeviationsInRad(it->second, zaxis));
    }
    ofstream ofstr("out_per_molec.txt");
    for(auto i = 0; i < molec_angles.size(); ++i){
        IO::write_vector(ofstr, molec_angles[i], "\t");
    }
    return 0;
}

