//
// Created by s1205916 on 19/06/15.
//
#include <iostream>
#include <algorithm>
#include <fstream>
#include <cmath>
#include "types.h"
#include "md_io.h"
#include "md_math.h"
#include "md_analysis.h"

using namespace mol_dyn;
using std::ifstream;
using std::string;
using std::vector;
using std::cout;
using std::endl;
using std::cerr;

int main(int argc, char** argv)
{

    if(argc != 4){
        cerr << "Usage: angularvariation <seedname>.md <no. atoms> <no. layers>" << endl;
        exit(EXIT_FAILURE);
    }
    string filename(argv[1]);
    int no_atoms = atoi(argv[2]);
    int no_layers = atoi(argv[3]);
    ifstream ifstr(filename);
    if(!ifstr){
        cerr << "Could not open file. " << endl << "Error code: " << ifstr.rdstate() << endl;
        cerr << "Usage: angularvariation <seedname>.md" << endl;
        exit(EXIT_FAILURE);
    }
    vector<crystal> md = IO::loadMD(ifstr, no_atoms, 0,0);
    analysis::identLayers(md, no_layers);
    math::convertUnits(md);
    vector<molCol> mCol = analysis::identMolCol(md, MolColMethod::CONST_SET);
    auto by_molec_id = analysis::sortByMolecule(mCol);
    coord zaxis(0,0,1);
    vector<vector<double>> molec_angles;
    for (auto it = by_molec_id.cbegin(); it != by_molec_id.cend(); ++it){
        molec_angles.push_back(analysis::thetaDeviationsInRad(it->second, zaxis));
    }
    vector<double> angles = mol_dyn::math::flatten(molec_angles);
    IO::write_vector(cout, angles);



    return 0;
}