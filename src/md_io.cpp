//
// Created by s1205916 on 18/06/15.
//

#include <vector>
#include <string>
#include <fstream>
#include <sstream>
#include "types.h"
#include "md_io.h"
#include "md_analysis.h"

using std::vector;
using std::string;
using std::ostream;
using std::ifstream;
using std::ofstream;
using std::stringstream;
using std::cout;
using std::cerr;
using std::endl;

ostream& mol_dyn::IO::write_molecular_bonds(std::ostream& out, const std::vector<molec>& molecules, const crystal& c){
    out << "%BLOCK NONLINEAR_CONSTRAINTS" << std::endl;

    for(std::vector<molec>::const_iterator i = molecules.begin(); i!= molecules.end(); ++i){
        atom a1 = i->a1;
        atom a2 = i->a2;
        Image_combined imgcbd = mol_dyn::analysis::boundaries_crossed(*i, c);
        out << "\t distance \t H " << a1.label + 1 << " " << imgcbd.a1 << " " << imgcbd.b1 << " " << imgcbd.c1 << "\t"
        << "  " << "H " << a2.label + 1 << " " << imgcbd.a2 << " " << imgcbd.b2 << " " << imgcbd.c2 << " "
        << endl;
    }
    out << "%ENDBLOCK NONLINEAR_CONSTRAINTS" << endl;
    return out;
}

mol_dyn::crystal mol_dyn::IO::loadCell(std::ifstream &in){
    string sbuf;
    crystal ret;
    vector<string> lines;
    while(getline(in, sbuf)){
        lines.push_back(sbuf);
    }


    for(auto i = 0; i < lines.size(); ++i){
        std::cout<< lines[i] << std::endl;
    }
    return ret;
}

vector<mol_dyn::crystal> mol_dyn::IO::loadMD(ifstream &in, int noAtoms, int ignore, int noIter) {
    double dbuf;
    string sbuf;
    vector<crystal> timeDep;
    int line = 0;
    int iter = 0;
    in >> sbuf >> sbuf >> sbuf >> sbuf; //header
    line++;
    if (sbuf != "header") {
        cerr << "Error: Unexpected format in header of MD file" << endl;
        exit(EXIT_FAILURE);
    }
    while (!in.eof()) {
        if (noIter > 0) if (iter >= noIter) break;
        if (ignore <= 0) iter++;
        crystal crs;
        int newNo;

        in >> crs.time;
        in >> dbuf >> dbuf >> dbuf >> sbuf >> sbuf; //varius energies
        if (in.eof()) {
            break;
        }
        line++;
        if (sbuf != "E") {
            cerr << "Error: Unexpected format at energy in MD file, line: " << line << endl;
            exit(EXIT_FAILURE);
        }
        in >> crs.temp >> sbuf >> sbuf;
        in >> crs.pres >> sbuf >> sbuf;
        in >> crs.v1.x >> crs.v1.y >> crs.v1.z >> sbuf >> sbuf;
        in >> crs.v2.x >> crs.v2.y >> crs.v2.z >> sbuf >> sbuf;
        in >> crs.v3.x >> crs.v3.y >> crs.v3.z >> sbuf >> sbuf;

        string firstname;
        while (1) {
            matrix dummy;
            string aux;
            in >> aux;

            if ((aux != "H") && (aux != "H:D")) {
                stringstream temp;
                temp << aux;
                temp >> dummy.m1.x;
                in >> dummy.m1.y >> dummy.m1.z >> aux >> aux;
                in >> dummy.m2.x >> dummy.m2.y >> dummy.m2.z >> aux >> aux;
                in >> dummy.m3.x >> dummy.m3.y >> dummy.m3.z >> aux >> aux;
            } else {
                firstname = aux;
                break;
            }
        }

        line += 5;
        for (int i = 0; i < noAtoms; i++) {
            atom a;
            a.partMolec = false;
            //different molecs have repeating labels
            //in >> a.name >> a.label >> a.crt.x >> a.crt.y >> a.crt.z >> sbuf >> sbuf;
            if (i == 0) {
                a.name = firstname;
            } else {
                in >> a.name;
            }
            in >> dbuf >> a.crt.x >> a.crt.y >> a.crt.z >> sbuf >> sbuf;
            a.label = i; //0 counting convention
            //FBALM: Does this mesh with the way that castep initialises? I don't think it does
            //TODO: figure out how this actually behaves
            line++;
            crs.list.push_back(a);
        }
        for (int i = 0; i < noAtoms; i++) {
            atom a;
            in >> a.name >> newNo >> a.crt.x >> a.crt.y >> a.crt.z >> sbuf >> sbuf;
            line++;
            crs.list[i].vel.x = a.crt.x;
            crs.list[i].vel.y = a.crt.y;
            crs.list[i].vel.z = a.crt.z;
        }
        for (int i = 0; i < noAtoms; i++) {
            atom a;
            in >> a.name >> newNo >> a.crt.x >> a.crt.y >> a.crt.z >> sbuf >> sbuf;
            line++;
            if (in.eof()) {
                break;
            }
            crs.list[i].frc.x = a.crt.x;
            crs.list[i].frc.y = a.crt.y;
            crs.list[i].frc.z = a.crt.z;
        }
        if (in.eof()) {
            break;
        }
        if (ignore <= 0) {
            timeDep.push_back(crs);
        } else {
            ignore--;
        }
    }
    return timeDep;
};

void mol_dyn::IO::createMOL(std::ofstream &out, std::vector<mol_dyn::crystal> &col) {
    for (auto i = 0; i < col.size(); i++) {
        out << col[i].list.size() << endl;
        out << "BOND INFORMATION" << endl;
        for (int j = 0; j < col[i].list.size(); j++) {
            out << col[i].list[j].name << "    "
            << col[i].list[j].crt.x << "  "
            << col[i].list[j].crt.y << "  "
            << col[i].list[j].crt.z << endl;
        }
    }
}

void mol_dyn::IO::createXYZ(std::ofstream &out, mol_dyn::crystal &col) {
    out << col.list.size() << endl;
    out << "average XYZ" << endl;
    for (vector<mol_dyn::crystal>::size_type j = 0; j < col.list.size(); j++) {
        out << col.list[j].name << "    "
        << col.list[j].crt.x << "  "
        << col.list[j].crt.y << "  "
        << col.list[j].crt.z << endl;
    }
}