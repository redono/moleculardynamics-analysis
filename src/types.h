#ifndef MOLDYN_TYPES_H
#define MOLDYN_TYPES_H

#include <string>
#include <vector>
#include <iostream>
#include <iomanip>

namespace mol_dyn {


    enum imaged {
        NO = 0,
        YES = 1
    };

    enum MolColMethod {
        CONST_SET,
        UPDATE_SET
    };

    enum hcp_stacking {
        ABAB,
        ABCABC
    };

    struct coord {
    public:
        coord(double xx, double yy, double zz) : x(xx), y(yy), z(zz) {}
        coord() : x(0), y(0), z(0) {}
        double x;
        double y;
        double z;

        coord& operator+=(const coord& other){
            x += other.x;
            y += other.y;
            z += other.z;
            return *this;
        }
        const coord operator+(const coord& other) const{
            coord ret = *this;
            ret += other;
            return ret;
        }
        coord& operator-=(const coord& other){
            x -= other.x;
            y -= other.y;
            z -= other.z;
            return *this;
        }
        const coord operator-(const coord& other) const {
            coord ret = *this;
            ret -= other;
            return ret;
        }
        coord& operator*=(const double& mult){
            x *= mult;
            y *= mult;
            z *= mult;
            return *this;
        }
        const coord operator*(const double& mult) const {
            coord ret = *this;
            ret *= mult;
            return ret;
        }
        coord& operator/=(const double& div){
            x /= div;
            y /= div;
            z /= div;
            return *this;
        }
        const coord operator/(const double& div) const {
            coord ret = *this;
            ret /= div;
            return ret;
        }

    };





    struct atom {
        std::string name;
        int label;
        int layer;    //applies for layered structs
        coord crt;    //cartesian
        coord vel;    //velocities
        coord frc;    //forces
        bool partMolec; //true if part of molecule
        /*void print(std::ostream& ostr) const {
            ostr << setw(10) << crt.x << " " <<
        }*/
    };

    struct molec {
        coord cm; //centre of mass
        int ID;
        bool checked;
        atom a1;
        atom a2;
        coord c1;    //transformation for atom 1
        coord c2;    //transformation for atom 2
    };

    struct crystal {
        std::vector<atom> list;
        double time;
        double temp;
        double pres;
        //unit vectors
        coord v1;
        coord v2;
        coord v3;
    };

    struct coord_spherical {
        double r, theta, phi;
    };


    struct matrix {
        coord m1;
        coord m2;
        coord m3;
    };

    struct molCol {
        double time;
        std::vector<molec> col;
    };

    struct timePt {
        double vel;
        double time;
    };

    struct timeCol {
        double startTime;
        int ID;
        std::vector<timePt> col;
    };

    struct molec_positions {
        int ID;
        std::vector<molec> positions;
    };

    struct Image_combined {
        imaged a1, b1, c1, a2, b2, c2;
    };

    struct molecule_state {
        coord centre_of_mass;
        coord_spherical sph;
    };
    typedef std::vector<molecule_state> molecules_snapshot;
    struct freqPt {
        double relInt;
        double imgInt;
        double freq;
        double Int;
    };

    inline std::ostream& operator<<(std::ostream &ostr, const coord &c) {
        ostr << std::setw(10) << c.x << " "
        << c.y << " " << c.z;
        return ostr;
    }

    inline std::ostream& operator<<(std::ostream &ostr, const coord_spherical &c) {
        ostr << std::setw(10) << c.r << " "
        << c.phi << " " << c.theta << std::flush;
        return ostr;
    }
    inline std::ostream& operator<<(std::ostream &ostr, const molecule_state& st) {
        ostr << st.centre_of_mass << " " << st.sph << std::flush;
        return ostr;
    }

}

#endif

