#include <iostream>
#include <fstream>
#include "md_io.h"
#include "md_math.h"
#include "md_analysis.h"

using std::cerr;
using std::endl;
using std::cout;
using std::ifstream;
using std::ofstream;
using std::ostream;
using std::string;
using std::vector;
using namespace mol_dyn;


int main(int argc, char** argv) {

    if(argc < 3 || argc > 4) {
        cout << "Usage: bondconstraints <file path>.md <# atoms> "<<
                " <optional: outfile> " << endl;
        exit(EXIT_SUCCESS);
    }
    cerr << "WARNING: NOT YET COMPLETE!" << endl;
    cerr << "SOFTWARE DOES NOT ACCOUNT FOR CROSS-UNIT CELL BOUNDARIES YET" << endl;
    string inpath(argv[1]);
    string outpath;
    if (argc == 4){
        outpath = string(argv[3]);
    } else {
        outpath = "out.txt";
    }
    int no_atoms(atoi(argv[2]));
    std::ifstream ifstream1(inpath);

    if(!ifstream1){
        cerr << "Could not open path " + inpath << endl;
        cerr << "Error code: " << ifstream1.rdstate() << endl;
        exit(EXIT_FAILURE);
    }

    vector<crystal> md = mol_dyn::IO::loadMD(ifstream1, no_atoms, 0, 0);
    math::convertUnits(md);
    vector<molCol> mCol = analysis::identMolCol(md, MolColMethod::CONST_SET);
    ofstream ofstr(outpath);

    if (ofstr) {
        IO::write_molecular_bonds(ofstr, mCol[0].col, md[0]);
    } else {
        cerr << "Could not open output file " << outpath << endl;
        cerr << "Error code: " << ofstr.rdstate() << endl;
        IO::write_molecular_bonds(cout, mCol[0].col, md[0]);
    }

    return 0;
}

