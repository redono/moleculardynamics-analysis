//
// Created by s1205916 on 18/06/15.
//
#ifndef MD_MATH_H
#define MD_MATH_H

#include <vector>
#include <initializer_list>
#include "types.h"
#include <algorithm>

namespace mol_dyn {

    namespace math {

        void convertUnits(std::vector<crystal> &col);

        double dist(const coord &vec);

        coord sum(const std::initializer_list<coord>);

        coord sum(const coord& c1, const coord& c2);

        double sum_coords(const coord &c);

        coord diff(const coord &c1, const coord &c2);

        coord mult(double a, const coord &c1);

        coord mult(const coord &c1, const coord &c2);

        double scalar(const coord &c1, const coord &c2);

        bool equal(double d1, double d2, double tolerance);

        bool equal(const coord &c1, const coord &c2);

        coord translate(const coord &prim, const coord &index, const coord &c1, const coord &c2, const coord &c3);

        coord prod(const coord &v1, const coord &v2);

        matrix inverse(const crystal &c);

        matrix transpose(const matrix &a);

        coord abs2intern(const coord &c, const crystal &crs);

        coord intern2abs(const coord &c, const crystal &crs);

        double projection(const molec &m, const crystal &c);

        double angle(const coord &, const coord &);

        coord vec_orientation(const molec& );

        double average(const std::vector<double>&);

        std::vector<double> average_deviation(const std::vector<std::vector<double>>&);

        //flatten vector of vectors (2d) into a single vector
        template<typename T>
        std::vector<T> flatten(std::vector<std::vector<T>> vec2d){
            using std::vector;
            vector<T> ret;
            //Since this array can be particularly large, it makes sense to
            //reserve enough space for it. This will improve run times by
            //skipping unnecessary allocations.
            ret.reserve(vec2d.size()*vec2d[0].size());
            for(auto i = vec2d.cbegin(); i != vec2d.cend(); ++i){
                for (auto j = i->cbegin(); j != i->cend(); ++j){
                    ret.push_back(*j);
                }
            }
            return ret;
        }
        coord to_unit_vector(const coord&);
        coord_spherical cart_to_spherical(const coord&);
        coord com_displacement(const molec &, const molec &, const crystal&);
        coord centre_of_mass(const molec&, const crystal&);
        double quadrupole_interaction(const molec &, const molec &, const crystal&);
        double my_quadrupole_expression(const molec&, const molec&, const crystal&);
        coord get_shortest_vector(const coord&, const coord&, const crystal&);

        double get_average_dot_product(const std::vector<molec> &, int stepDiff);
    }
}
#endif