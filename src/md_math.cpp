//
// Created by s1205916 on 18/06/15.
//

#include "types.h"
#include <cmath>
#include "md_math.h"
#include "md_analysis.h"

using std::vector;
using mol_dyn::coord;
using mol_dyn::crystal;
using mol_dyn::coord_spherical;
using mol_dyn::matrix;
using mol_dyn::analysis::perVec;
using std::cout;
using std::endl;

void mol_dyn::math::convertUnits(std::vector<crystal> &col) {
    //41.3414 = rts/fs ["random" time step to femto second]
    for (auto i = 0; i < col.size(); i++) {
        col[i].time = col[i].time / 41.3414;
        col[i].v1 = mult(0.529177249, col[i].v1);
        col[i].v2 = mult(0.529177249, col[i].v2);
        col[i].v3 = mult(0.529177249, col[i].v3);
        for (auto j = 0; j < col[i].list.size(); j++) {
            //to Angstrom
            col[i].list[j].crt = mult(0.529177249, col[i].list[j].crt);
            //to Angstrom/fs
            col[i].list[j].vel.x = col[i].list[j].vel.x * 0.529177249 * 41.3414;
            col[i].list[j].vel.y = col[i].list[j].vel.y * 0.529177249 * 41.3414;
            col[i].list[j].vel.z = col[i].list[j].vel.z * 0.529177249 * 41.3414;
            //to Angstrom/fs2
            col[i].list[j].frc.x = col[i].list[j].frc.x * 0.529177249 * 41.3414 * 41.3414;
            col[i].list[j].frc.y = col[i].list[j].frc.y * 0.529177249 * 41.3414 * 41.3414;
            col[i].list[j].frc.z = col[i].list[j].frc.z * 0.529177249 * 41.3414 * 41.3414;
        }
    }
};

coord mol_dyn::math::sum(const std::initializer_list<coord> coords){
    coord ret;
    for(auto i = coords.begin(); i != coords.end(); ++i){
        ret.x += i->x;
        ret.y += i->y;
        ret.z += i->z;
    }
    return ret;
};

coord mol_dyn::math::sum(const coord& c1, const coord& c2){
    coord ret;
    ret.x = c1.x+c2.x;
    ret.y = c1.y+c2.y;
    ret.z = c1.z+c2.z;
    return ret;
}

double mol_dyn::math::dist(const coord &vec) {
    return sqrt(pow(vec.x, 2.0) + pow(vec.y, 2.0) + pow(vec.z, 2.0));
};


double mol_dyn::math::sum_coords(const coord &c) {
    double summation = c.x + c.y + c.z;
    return summation;
};

coord mol_dyn::math::diff(const coord &c1, const coord &c2) {
    coord c;
    c.x = c2.x - c1.x;
    c.y = c2.y - c1.y;
    c.z = c2.z - c1.z;
    return c;
};

coord mol_dyn::math::mult(double a, const coord &c1) {
    coord c;
    c.x = a * c1.x;
    c.y = a * c1.y;
    c.z = a * c1.z;
    return c;
};

coord mol_dyn::math::mult(const coord &c1, const coord &c2) {
    coord c;
    c.x = c1.x * c2.x;
    c.y = c1.y * c2.y;
    c.z = c1.z * c2.z;
    return c;
};

double mol_dyn::math::scalar(const coord &c1, const coord &c2) {
    coord scal = mult(c1, c2);
    return sum_coords(scal);
};

bool mol_dyn::math::equal(double d1, double d2, double tolerance = 0.00001) {
    return (fabs(d1 - d2) < fabs(tolerance));
};

bool mol_dyn::math::equal(const coord &c1, const coord &c2) {
    bool egal = true;
    if (fabs(c1.x - c2.x) > 0.001) {
        egal = false;
    }
    if (fabs(c1.y - c2.y) > 0.001) {
        egal = false;
    }
    if (fabs(c1.z - c2.z) > 0.001) {
        egal = false;
    }
    return egal;
};

coord mol_dyn::math::translate(const coord &prim, const coord &index, const coord &c1, const coord &c2, const coord &c3) {
    coord sec = prim;
    coord vec1 = mult(index.x, c1);
    coord vec2 = mult(index.y, c2);
    coord vec3 = mult(index.z, c3);
    sec = sum({sec, vec1});
    sec = sum({sec, vec2});
    sec = sum({sec, vec3});
    return sec;
};

coord mol_dyn::math::prod(const coord &v1, const coord &v2) {
    coord product;
    product.x = v1.y * v2.z - v1.z * v2.y;
    product.y = v1.z * v2.x - v1.x * v2.z;
    product.z = v1.x * v2.y - v1.y * v2.x;
    return product;
};

matrix mol_dyn::math::inverse(const crystal &c) {
    double det = 0;
    det = c.v1.x * c.v2.y * c.v3.z + c.v1.y * c.v2.z * c.v3.x + c.v1.z * c.v2.x * c.v3.y -
          c.v1.z * c.v2.y * c.v3.x - c.v1.y * c.v2.x * c.v3.z - c.v1.x * c.v2.z * c.v3.y;
    matrix inv;
    inv.m1.x = (c.v2.y * c.v3.z - c.v2.z * c.v3.y) / det;
    inv.m1.y = (c.v1.z * c.v3.y - c.v1.y * c.v3.z) / det;
    inv.m1.z = (c.v1.y * c.v2.z - c.v1.z * c.v2.y) / det;
    inv.m2.x = (c.v2.z * c.v3.x - c.v2.x * c.v3.z) / det;
    inv.m2.y = (c.v1.x * c.v3.z - c.v1.z * c.v3.x) / det;
    inv.m2.z = (c.v1.z * c.v2.x - c.v1.x * c.v2.z) / det;
    inv.m3.x = (c.v2.x * c.v3.y - c.v2.y * c.v3.x) / det;
    inv.m3.y = (c.v1.y * c.v3.x - c.v1.x * c.v3.y) / det;
    inv.m3.z = (c.v1.x * c.v2.y - c.v1.y * c.v2.x) / det;
    return inv;
};

matrix mol_dyn::math::transpose(const matrix &a) {
    matrix b;
    b.m1.x = a.m1.x;
    b.m1.y = a.m2.x;
    b.m1.z = a.m3.x;
    b.m2.x = a.m1.y;
    b.m2.y = a.m2.y;
    b.m2.z = a.m3.y;
    b.m3.x = a.m1.z;
    b.m3.y = a.m2.z;
    b.m3.z = a.m3.z;
    return b;
};

coord mol_dyn::math::abs2intern(const coord &c, const crystal &crs) {
    coord in;
    coord tmp1, tmp2, tmp3;
    matrix v = inverse(crs);
    //using that (A^T)^-1 = (A^-1)^T
    matrix vr = transpose(v);
    tmp1 = mult(c, vr.m1);
    tmp2 = mult(c, vr.m2);
    tmp3 = mult(c, vr.m3);
    in.x = sum_coords(tmp1);
    in.y = sum_coords(tmp2);
    in.z = sum_coords(tmp3);
    return in;
};

coord mol_dyn::math::intern2abs(const coord &c, const crystal &crs) {
    coord abs;
    coord tmp1, tmp2, tmp3;
    matrix v;
    v.m1 = crs.v1;
    v.m2 = crs.v2;
    v.m3 = crs.v3;
    matrix vr = transpose(v);
    tmp1 = mult(c, vr.m1);
    tmp2 = mult(c, vr.m2);
    tmp3 = mult(c, vr.m3);
    abs.x = sum_coords(tmp1);
    abs.y = sum_coords(tmp2);
    abs.z = sum_coords(tmp3);
    return abs;
};

double mol_dyn::math::projection(const molec &m, const crystal &c) {
    double vel = 0;
    coord dummy;
    coord c1 = mol_dyn::analysis::perVec(m.a2.crt, m.a1.crt, c, dummy);
    coord c2 = mol_dyn::analysis::perVec(m.a1.crt, m.a2.crt, c, dummy);
    double d1 = dist(c1);
    double d2 = dist(c2);
    c1 = mult(1.0 / d1, c1);
    c2 = mult(1.0 / d2, c2);
    vel += scalar(m.a1.vel, c1);
    vel += scalar(m.a2.vel, c2);
    return vel;
};
double mol_dyn::math::angle(const coord &c1, const coord &c2) {
    return acos(scalar(c1, c2) / (dist(c1) * dist(c2)));
}

coord mol_dyn::math::vec_orientation(const molec& m) {
    //DIFF GOES FROM A1 ____TO____ A2
    return diff(m.a1.crt, m.a2.crt);
}

double mol_dyn::math::average(const vector<double>& vec){
    if (vec.size() == 0){
        return 0.0;
    }
    double sz = static_cast<double> (vec.size());
    double total = 0;

    //Divide at each step to prevent over/underflow when handling large
    //numbers in the vector
    for(auto i = 0; i < vec.size(); ++i){
        total += (vec[i]/sz);
    }
    return total;
}
coord_spherical mol_dyn::math::cart_to_spherical(const coord &c) {
    coord_spherical ret;
    ret.r = dist(c);
    ret.phi = atan2(c.y,c.x);
    ret.theta = atan2(sqrt(c.x*c.x + c.y*c.y),c.z);
    return ret;
}

coord mol_dyn::math::to_unit_vector(const coord& c){
    double len = dist(c);
    coord ret;
    ret.x = c.x/len;
    ret.y = c.y/len;
    ret.z = c.z/len;
    return ret;
}

coord mol_dyn::math::centre_of_mass(const mol_dyn::molec& m, const crystal& c){
    coord a1 = m.a1.crt;
    coord a2 = m.a2.crt;
    coord dummy;
    coord disp = analysis::perVec(a1, a2, c, dummy);

    coord com = a1 + (disp * 0.5);
    return com;
}

//Not 100% bug-free! This code assumes that all molecules are identified
//at all points, and that there are a constant number of molecules in the
//ensemble. EXPERIMENTAL!
std::vector<double> mol_dyn::math::average_deviation(const vector<vector<double>>& deviations) {
    std::vector<double> ret(deviations[0].size(), 0);
    double sz = static_cast<double>(deviations.size());
    //Bit of an awkward loop, but since vectors are in the C++ standard
    //defined to be contiguous, handling the loop this way could potentially
    //speed up the code significantly as it use caches much more efficiently
    for(auto i = 0; i < deviations.size(); ++i){
        for(auto j = 0; j<deviations[i].size(); ++j){
            ret[j] += deviations[i][j]/sz;
        }
    }
    return ret;
}

//I honestly do not know what this means. It just has symbols, but it does not make any physical sense to me.
//Formula as used by Graeme: quad = 35*(dotir*dotjr)**2-20* (dotir*dotij*dotjr) - 5 * (dotjr*dotjr + dotir*dotir) +2*dotij*dotij +1
double  mol_dyn::math::quadrupole_interaction(const molec & m1, const molec & m2, const crystal& c){
    coord cm1 = centre_of_mass(m1, c);
    coord cm2 = centre_of_mass(m2, c);
    coord dummy;
    if (dummy.x || dummy.y || dummy.z){
        coord intcm2 = math::abs2intern(cm2, c);
        /**cout << "dummy nonzero" << endl;
        cout << "before:" << endl;
        cout << cm1 << endl;
        cout << cm2 << endl;
        cout << "internal" << endl;
        cout << intcm2 << endl;
        cout << "dummy" << endl;
        cout << dummy << endl; **/
        intcm2 += dummy;
      //  cout << "Second plus dummy" << endl;
      //  cout << intcm2 << endl;
        cm2 = math::intern2abs(intcm2, c);
      //  cout << "new coords:" << endl;
      //  cout << cm1 << endl;
      //  cout << cm2 << endl;
      //  cout << endl << endl;
    }
    coord r = analysis::perVec(cm1, cm2, c, dummy);
    coord unit_vec_1 = to_unit_vector(vec_orientation(m1));
    coord unit_vec_2 = to_unit_vector(vec_orientation(m2));

    double dotir = scalar(unit_vec_1, r);
    double dotij = scalar(unit_vec_1, unit_vec_2);
    std::cout << dotij<< endl;
    return dotij;
    double dotjr = scalar(unit_vec_2, r);

    double quad = 35 * (dotir * dotjr) * (dotir * dotjr) - 20 * (dotir * dotij * dotjr)
                  - 5 * (dotjr * dotjr + dotir * dotir) + 2 * dotij * dotij + 1;
    //EXPERIMENTAL::
    //quad /= pow(dist(r), 5.0);
    return quad;
}

//This is just the straight up electrical interaction. I do not know whether to use
// some kind of more complicated quadrupole interaction.
//However, the one that Graeme was using in his code has no physical meaning to me
double mol_dyn::math::my_quadrupole_expression(const molec& m1, const molec& m2, const crystal& c) {
    typedef mol_dyn::coord vec;
    vec R = math::com_displacement(m1, m2, c);
    vec k1 = math::vec_orientation(m1);
    vec k2 = math::vec_orientation(m2);

    double E14, E15, E16, E24, E25, E26, E34, E35, E36;
    E14 =  1.0 / dist(R - (k2/2.0) + (k1/2.0));
    E15 = -2.0 / dist(R + (k1/2.0));
    E16 =  1.0 / dist(R + (k2/2.0) + (k1/2.0));
    E24 = -2.0 / dist(R - (k2/2.0));
    E25 =  4.0 / dist(R);
    E26 = -2.0 / dist(R + (k2/2.0));
    E34 =  1.0 / dist(R + (k2/2.0) - (k1/2.0));
    E35 = -2.0 / dist(R - (k1/2.0));
    E36 =  1.0 / dist(R - (k2/2.0) - (k1/2.0));
    double sum_e = E14+E15+E16+E24+E25+E26+E34+E35+E36;

    //Prefactor: e^2/4pi epsilon 0
    double prefactor_eV_A = 14.39964485;
    // In case I ever want to change units
    // double prefactor_Jm = 2.30707735E-28;
    return sum_e*prefactor_eV_A;

}

coord mol_dyn::math::get_shortest_vector(const coord& c1, const coord& c2, const crystal& cryst){
    coord a1int = abs2intern(c1, cryst);
    coord a2int = abs2intern(c2, cryst);
    coord c1cpyint(c1);
    coord c2cpyint(c2);

    double a_x_dist = fabs(a1int.x - a2int.x);
    //image of first atom in a+1 is closer than other atom
    bool change = true;
    while (change){
        change = false;
        if (fabs((a1int.x + 1) - a2int.x) < a_x_dist){
            c1cpyint.x += 1;
            change = true;
        } //If the second one is closer to the first in a+1 in
        else if (fabs(a1int.x - (a2int.x+1)) < a_x_dist){
            c2cpyint.x += 1;
            change = true;
        }
        double a_y_dist = fabs(a1int.y - a2int.y);
        //image of first atom in a+1 is closer than other atom
        if (fabs((a1int.y + 1) - a2int.y) < a_y_dist){
            c1cpyint.y += 1;
            change = true;
        } //If the second one is closer to the first in a+1 in
        else if (fabs(a1int.y - (a2int.y+1)) < a_y_dist){
            c1cpyint.y += 1;
            change = true;
        }
        double a_z_dist = fabs(a1int.z - a2int.z);
        //image of first atom in a+1 is closer than other atom
        if (fabs((a1int.z + 1) - a2int.z) < a_z_dist){
            c1cpyint.z += 1;
            change = true;
        } //If the second one is closer to the first in a+1 in
        else if (fabs(a1int.z - (a2int.z+1)) < a_z_dist){
            c2cpyint.z += 1;
            change = true;
        }
    }

    coord c1cpy = intern2abs(c1cpyint,cryst);
    coord c2cpy = intern2abs(c2cpyint,cryst);

    return diff(c1cpy, c2cpy);
}

double mol_dyn::math::get_average_dot_product(const vector<molec>& positions, int stepDiff){
    auto sz = positions.size();
    double total_dot = 0;
    for(vector<molec>::size_type i = 0; i + stepDiff < sz; ++i){
        auto first = to_unit_vector(vec_orientation(positions[i]));
        auto second = to_unit_vector(vec_orientation(positions[i + stepDiff]));
        total_dot += scalar(first, second);
    }
    total_dot /= static_cast<double>(sz);
    return total_dot;
}

coord mol_dyn::math::com_displacement(const mol_dyn::molec &molec1, const mol_dyn::molec &molec2, const crystal& c) {
    coord com1 = centre_of_mass(molec1, c);
    coord com2 = centre_of_mass(molec2, c);
    coord r = diff(com1, com2);
    return r;
}
