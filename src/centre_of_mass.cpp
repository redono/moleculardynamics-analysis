//
// Created by s1205916 on 09/07/15.
//

#include <iostream>
#include <fstream>
#include "md_io.h"
#include "md_math.h"
#include "md_analysis.h"

using std::cerr;
using std::endl;
using std::cout;
using std::ifstream;
using std::ofstream;
using std::ostream;
using std::string;
using std::vector;
using namespace mol_dyn;


int main(int argc, char** argv) {

    if(argc != 3) {
        cout << "Usage: " << argv[0] << " <file path>.md <# atoms> " << endl;
        exit(EXIT_SUCCESS);
    }
    string inpath(argv[1]);
    int no_atoms(atoi(argv[2]));

    std::ifstream ifstr(inpath);

    if(!ifstr){
        cerr << "Could not open path " + inpath << endl;
        cerr << "Error code: " << ifstr.rdstate() << endl;
        exit(EXIT_FAILURE);
    }

    vector<crystal> md = mol_dyn::IO::loadMD(ifstr, no_atoms, 0, 0);
    math::convertUnits(md);
    vector<molCol> mCol = analysis::identMolCol(md, MolColMethod::UPDATE_SET);
    cout << "C1 :: A1.crt :: COM :: C2 :: A2.crt" << endl;
    vector<molec>& molecules = mCol[0].col;
    crystal& c = md[0];
    std::cout << molecules.size() << endl;
    for(auto i = 0; i != molecules.size(); ++i){
        for(auto j = i + 1; j != molecules.size(); ++j){
            coord a = math::centre_of_mass(molecules[i]);
            coord b = math::centre_of_mass(molecules[j]);
            coord ret = math::get_shortest_vector(a, b, c);
            std::cout << ret << endl;
        }
    }

    return 0;
}