//
// Created by s1205916 on 14/08/15.
//

#include "types.h"
#include "md_analysis.h"
#include "md_math.h"
#include "md_io.h"
#include <fstream>
#include <thread>
#include <vector>
#include <iostream>

using std::cerr;
using std::cout;
using std::ifstream;
using namespace mol_dyn;
using std::vector;
using std::endl;
using std::string;
using std::thread;

int main(int argc, char** argv){
    //Check for the necessary command line arguments
    //Possible todo: allow for command line flags-style arguments
    if(argc != 3){
        cerr << "Usage: " << argv[0] << " <seedname>.md <# atoms>" << endl;
        exit(EXIT_FAILURE);
    }

    //Use the command line arguments. The file needs to be opened propery for the program to run.
    //While it would be possible to read them in a simple stdin/redirect kind of way, this versatility
    //is currently not required. By writing the state to stderr the error should be descriptive enough.
    string filename(argv[1]);
    int no_atoms = atoi(argv[2]);
    int no_layers = 6;
    ifstream ifstr(filename);
    if(!ifstr){
        cerr << "Could not open file: " << filename << endl << "Error code: " << ifstr.rdstate() << endl;
        cerr << "Usage: angularvariation <seedname>.md" << endl;
        exit(EXIT_FAILURE);
    }

    //Read and process the MD file to identify molecules and such
    vector<crystal> md = IO::loadMD(ifstr, no_atoms, 0,0);
    //analysis::identLayers(md, no_layers);
    math::convertUnits(md);
    vector<molCol> mCol = analysis::identMolCol(md, MolColMethod::CONST_SET);

    //Sort the molecules by their ID throughout the whole simulation
    //they are put into a hashmap indexed by the molecule index.
    std::unordered_map<int, molec_positions> by_molec_id = analysis::sortByMolecule(mCol);

    //unordered hashmaps have no natural traversal order: insertions or deletions can unpredictably modify ordering
    //so just loop over and do whatever.

    for (auto t_diff = 1; t_diff < mCol.size()/2.0; t_diff += 1){
        double val = 0;

        for (auto kv = by_molec_id.begin(); kv != by_molec_id.end(); ++kv){
            val += math::get_average_dot_product(kv->second.positions, t_diff);
        }
        val /= static_cast<double>(no_atoms);
        std::cout << t_diff << " " << val << std::endl;
    }

    return 0;
}