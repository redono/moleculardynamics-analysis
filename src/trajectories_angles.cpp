//
// Created by s1205916 on 19/08/15.
// This program writes positions of molecular centres of mass and their r, theta, phi orientations
#include <iostream>
#include <algorithm>
#include <sstream>
#include <fstream>
#include <cmath>
#include "types.h"
#include "md_io.h"
#include "md_math.h"
#include "md_analysis.h"

using namespace mol_dyn;
using std::ifstream;
using std::string;
using std::vector;
using std::cout;
using std::endl;
using std::cerr;
using std::stringstream;
using std::ofstream;

int main(int argc, char** argv) {

    if (argc != 3) {
        cerr << "Usage: "<< argv[0] << " <seedname>.md <no. atoms>" << endl;
        exit(EXIT_FAILURE);
    }
    string filename(argv[1]);
    int no_atoms = atoi(argv[2]);
    ifstream ifstr(filename);
    if (!ifstr) {
        cerr << "Could not open file. " << endl << "Error code: " << ifstr.rdstate() << endl;
        cerr << "Usage: angularvariation <seedname>.md" << endl;
        exit(EXIT_FAILURE);
    }
    vector<crystal> md = IO::loadMD(ifstr, no_atoms, 0, 0);
    analysis::identLayers(md, 6);
    math::convertUnits(md);
    vector<molCol> mCol = analysis::identMolCol(md, MolColMethod::CONST_SET);

    vector<molecules_snapshot> molec_angles;
    for (auto i = 0; i != mCol.size(); ++i){
        crystal& c = md[i];
        molec_angles.push_back(analysis::get_molecule_states(mCol[i], c));
    }
    for(int i = 0; i < md.size(); ++i){
        stringstream sstr;
        sstr << "out" << i << ".txt";
        ofstream ofstr(sstr.str());
        if (ofstr) {
            ofstr << "x y z r phi theta" << endl;
            IO::write_vector(ofstr, molec_angles[i], "\n");
        } else {
            std::cout << "x y z r phi theta" << endl;
            IO::write_vector(cout, molec_angles[i], "\n");
        }
    }



    return 0;
}