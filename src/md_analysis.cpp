//
// Created by s1205916 on 18/06/15.
//

#include "md_analysis.h"
#include "md_math.h"
#include "md_io.h"
#include <cmath>

using mol_dyn::coord;
using mol_dyn::matrix;
using std::cout;
using std::endl;
using mol_dyn::molCol;
using mol_dyn::molec;
using namespace mol_dyn;
using std::vector;
using std::unordered_map;

coord mol_dyn::analysis::perVec(const coord &cm1, const coord &cm2, const crystal &c, coord &index) {
    //returns pointer 1->2 (shortest between within cell and boundary)
    coord vec;
    coord ncm1 = math::abs2intern(cm1, c);
    coord ncm2 = math::abs2intern(cm2, c);
    int maxTrans = 1;
    double min_dist = 100;
    for (auto m = -maxTrans; m <= maxTrans; m++) {
        double ndist = fabs(m + ncm2.x - ncm1.x);
        if (ndist < min_dist) {
            min_dist = ndist;
            index.x = m;
        }
    }
    min_dist = 100;
    for (auto m = -maxTrans; m <= maxTrans; m++) {
        double ndist = fabs(m + ncm2.y - ncm1.y);
        if (ndist < min_dist) {
            min_dist = ndist;
            index.y = m;
        }
    }
    min_dist = 100;
    for (auto m = -maxTrans; m <= maxTrans; m++) {
        double ndist = fabs(m + ncm2.z - ncm1.z);
        if (ndist < min_dist) {
            min_dist = ndist;
            index.z = m;
        }
    }
    coord trafo1 = math::mult(index.x, c.v1);
    coord trafo2 = math::mult(index.y, c.v2);
    coord trafo3 = math::mult(index.z, c.v3);
    auto coords = {cm2, trafo1, trafo2, trafo3};
    coord trueCm2 = math::sum(coords);
    vec = math::diff(trueCm2, cm1);
    return vec;
}

double mol_dyn::analysis::perDist(const coord &c1, const coord &c2, const crystal &crs, coord &index) {
    coord v = perVec(c1, c2, crs, index);
    return math::dist(v);
}

void mol_dyn::analysis::identLayers(mol_dyn::crystal& crystal, int noLayers){
    return;
}

void mol_dyn::analysis::identLayers(std::vector<mol_dyn::crystal>& md, int noLayers) {
    double zmax = md[0].list[0].crt.z;
    double zmin = md[0].list[0].crt.z;

    for (auto i = 1; i < md[0].list.size(); i++) {
        if (md[0].list[i].crt.z > zmax) {
            zmax = md[0].list[i].crt.z;
        } else if (md[0].list[i].crt.z < zmin) {
            zmin = md[0].list[i].crt.z;
        }
    }
    double diff = (zmax - zmin) / (noLayers - 1);
    zmin = zmin - 0.5 * diff;
    for (auto i = 0; i < md[0].list.size(); i++) {
        for (auto l = 1; l <= noLayers; l++) {
            if ((md[0].list[i].crt.z >= zmin + (l - 1) * diff) &&
                (md[0].list[i].crt.z < zmin + l * diff)) {
                for (auto t = 0; t < md.size(); t++) {
                    md[t].list[i].layer = l;
                }
                break;
            }
        }
    }
}

int mol_dyn::analysis::IDMol(const molec &m, const int no_atoms) {
    int ID = 0;
    if (m.a1.label <= m.a2.label) {
        ID = m.a1.label * no_atoms + m.a2.label;
    } else {
        ID = m.a2.label * no_atoms + m.a1.label;
    }
    return ID;
}

molCol mol_dyn::analysis::identMol(mol_dyn::crystal &c, const int no_atoms) {
    molCol mCol;
    mCol.time = c.time;
    for (auto i = 0; i < c.list.size(); i++) {
        if (c.list[i].partMolec) {
            continue;
        }
        double min_dist = 100.0;
        int min_nb = -1;
        coord min_trafo;
        coord min_dummy;
        for (auto j = 0; j < c.list.size(); j++) {
            if (c.list[j].partMolec) {
                continue;
            }
            if (i == j) {
                continue;
            }
            coord trafo;
            coord dummy = perVec(c.list[i].crt, c.list[j].crt, c, trafo);
            if (math::dist(dummy) <= min_dist) {
                min_dist = math::dist(dummy);
                min_nb = j;
                min_trafo = trafo;
                min_dummy = dummy;
            }
        }
        //threshold for bond
        if (min_dist <= 1.1) {
            molec m;
            m.c1.x = 0;
            m.c1.y = 0;
            m.c1.z = 0;
            m.c2.x = min_trafo.x;
            m.c2.y = min_trafo.y;
            m.c2.z = min_trafo.z;
            m.a1 = c.list[i];
            m.a2 = c.list[min_nb];
            c.list[i].partMolec = true;
            c.list[min_nb].partMolec = true;
            //find center of mass
            min_dummy = math::mult(0.5, min_dummy);
            //  do not use CM for Raman
            m.cm = math::sum({c.list[i].crt, min_dummy});
            //convert to internal coordinates
            m.cm = math::abs2intern(m.cm, c);
            //reduce to unit cell
            if (m.cm.x < 0.0) {
                m.cm.x = m.cm.x + 1.0;
            }
            if (m.cm.x > 1.0) {
                m.cm.x = m.cm.x - 1.0;
            }
            if (m.cm.y < 0.0) {
                m.cm.y = m.cm.y + 1.0;
            }
            if (m.cm.y > 1.0) {
                m.cm.y = m.cm.y - 1.0;
            }
            if (m.cm.z < 0.0) {
                m.cm.z = m.cm.z + 1.0;
            }
            if (m.cm.z > 1.0) {
                m.cm.z = m.cm.z - 1.0;
            }
            m.ID = IDMol(m, no_atoms);
            m.checked = false;
            //convert back to absolute coordinates
            m.cm = math::intern2abs(m.cm, c);
            mCol.col.push_back(m);
            //        cerr << m.a1.layer << "  " << m.a2.layer << endl;
        }
    }
    return mCol;
}

std::vector<mol_dyn::molCol> mol_dyn::analysis::identMolCol(std::vector<mol_dyn::crystal> &md, const MolColMethod method) {
    std::vector<molCol> mCol;
    int no_atoms = static_cast<int>(md[0].list.size());
    molCol m = identMol(md[0], no_atoms);
    mCol.push_back(m);
    for (auto i = 1; i < md.size(); i++) {
        if (method == MolColMethod::CONST_SET) {
            //keep the same set of molecules
            //and update crt, vel, frc
            m.time = md[i].time;
            for (auto j = 0; j < m.col.size(); j++) {
                m.col[j].a1.crt = md[i].list[m.col[j].a1.label].crt;
                m.col[j].a2.crt = md[i].list[m.col[j].a2.label].crt;
                m.col[j].a1.vel = md[i].list[m.col[j].a1.label].vel;
                m.col[j].a2.vel = md[i].list[m.col[j].a2.label].vel;
                m.col[j].a1.frc = md[i].list[m.col[j].a1.label].frc;
                m.col[j].a2.frc = md[i].list[m.col[j].a2.label].frc;
            }
        }
        if (method == MolColMethod::UPDATE_SET) {
            //update set of molecules at each step
            //identify new molecules
            m = identMol(md[i], no_atoms);
            m.time = md[i].time;
        }
        mCol.push_back(m);
    }
    return mCol;
}

//returns continuous a list of projected velocities
timeCol mol_dyn::analysis::extractVel(const vector<mol_dyn::molCol> &mCol, const vector<mol_dyn::crystal> &crs) {
    timeCol tcol;
    tcol.startTime = mCol[0].time;
    tcol.ID = 0;
    timePt pt;
    for (auto i = 0; i < mCol.size(); i++) {
        pt.vel = 0;
        pt.time = mCol[i].time;
        for (auto j = 0; j < mCol[i].col.size(); j++) {
//       if (mCol[i].col[j].a1.layer == 2) continue;
//       if (mCol[i].col[j].a1.layer == 4) continue;
//       if (!(mCol[i].col[j].a1.name == "H" && mCol[i].col[j].a2.name == "H")) continue;
//       if (!(mCol[i].col[j].a1.name == "H:D" && mCol[i].col[j].a2.name == "H:D")) continue;
            pt.vel += mol_dyn::math::projection(mCol[i].col[j], crs[i]);
        }
        tcol.col.push_back(pt);
    }
    return tcol;
}

vector<freqPt> mol_dyn::analysis::DFT(const vector<timePt> &vec, double T) {
    vector<freqPt> ftVec;
    double size = static_cast<double>(vec.size());
//   double T = vec[1].time - vec[0].time; //[fs]
    double F = 1.0 / T;
    for (auto k = 0; k < vec.size(); k++) {
        freqPt pt;
        pt.freq = k / (T * size);
        //from Peta Hz to cm-1
        pt.freq = pt.freq * pow(10.0, 5) / 3.0;
        pt.relInt = 0;
        pt.imgInt = 0;
        for (auto n = 0; n < size; n++) {
            pt.relInt += vec[n].vel * cos(2.0 * M_PI * static_cast<double>(k) * static_cast<double>(n) / size);
            pt.imgInt -= vec[n].vel * sin(2.0 * M_PI * static_cast<double>(k) * static_cast<double>(n) / size);
        }
        ftVec.push_back(pt);
    }
    return ftVec;
}

double mol_dyn::analysis::XRD(const crystal &c, int k, int l, int m) {
    double rel = 0.0;
    double img = 0.0;
    for (auto i = 0; i < c.list.size(); i++) {
        coord frac = math::abs2intern(c.list[i].crt, c);
        rel += 0.5 * cos(2.0 * M_PI * (static_cast<double>(k) * frac.x + static_cast<double>(l) * frac.y + static_cast<double>(m) * frac.z));
        img += 0.5 * sin(2.0 * M_PI * (static_cast<double>(k) * frac.x + static_cast<double>(l) * frac.y + static_cast<double>(m) * frac.z));
    }
    return sqrt(rel * rel + img * img);
}

freqPt mol_dyn::analysis::interp(const freqPt &inf, const freqPt &sup, double fr) {
    freqPt newPt;
    newPt.freq = fr;
    double diff13 = sup.freq - inf.freq;
    double diff12 = fr - inf.freq;
    double diff23 = sup.freq - fr;
    //wrong
//     newPt.relInt = (sup.relInt * diff12 + inf.relInt * diff23) / diff13;
//     newPt.imgInt = (sup.imgInt * diff12 + inf.imgInt * diff23) / diff13;
    //right
    newPt.relInt = sup.relInt * diff12 + inf.relInt * diff23 / diff13;
    newPt.imgInt = sup.imgInt * diff12 + inf.imgInt * diff23 / diff13;
    return newPt;
}

//good -> bad res ?
vector<freqPt> mol_dyn::analysis::combineMaxRes(const vector<vector<freqPt>> &vecCol) {
    vector<freqPt> newVec;
    vector<int> indices;
    //identify the largest vector
    auto max_size = vecCol[0].size();
    int max_vec = 0;
    for (auto i = 1; i < vecCol.size(); i++) {
        if (vecCol[i].size() > max_size) {
            max_size = vecCol[i].size();
            max_vec = i;
        }
    }
    newVec = vecCol[max_vec];
    //collect all information at maximum resolution
//   for (int i = 0; i < newVec.size(); i++) {
//     for (int j = 0; j < vecCol.size(); j++) {
//       if (j == max_vec)
// 	continue;
//       bool found = false;
//       for (int k = 0; k < vecCol[j].size(); k++) {
// 	if (vecCol[j][k].freq <= newVec[i].freq) {
// 	  if ((k + 1) >= vecCol[j].size()) {
// 	    break;
// 	  }
// 	  if (vecCol[j][k+1].freq > newVec[i].freq) {
// 	    found = true;
// 	    freqPt newPt = interp(vecCol[j][k], vecCol[j][k+1], newVec[i].freq);
// 	    newVec[i].relInt += newPt.relInt;
// 	    newVec[i].imgInt += newPt.imgInt;
// 	    break;
// 	  }
// 	}
//       }
//     }
//   }
    return newVec;
}

vector<freqPt> mol_dyn::analysis::combineMinRes(const vector<vector<freqPt>> &vecCol) {

    vector<int> indices;
    //identify the smallest vector
    auto min_size = vecCol[0].size();
    int min_vec = 0;
    indices.push_back(0);
    for (int i = 1; i < vecCol.size(); i++) {
        indices.push_back(0);
        if (vecCol[i].size() < min_size) {
            min_size = vecCol[i].size();
            min_vec = i;
        }
    }
    vector<freqPt> newVec(vecCol[min_vec]);
    indices[min_vec] = (int)min_size - 1;
    //collect all information at minimum resolution
    for (auto i = 0; i < newVec.size(); i++) {
        for (auto j = 0; j < vecCol.size(); j++) {
            for (auto k = indices[j]; k < vecCol[j].size(); k++) {
                if (vecCol[j][k].freq <= newVec[i].freq) {
                    newVec[i].relInt += vecCol[j][k].relInt;
                    newVec[i].imgInt += vecCol[j][k].imgInt;
                    indices[j]++;
                } else {
                    break;
                }
            }
        }
    }
    return newVec;
}

vector<freqPt> mol_dyn::analysis::DFTCol(const vector<timeCol> &vecCol, double prag) {
    bool first = true;
    vector<vector<freqPt> > colft;
    double T = vecCol[0].col[1].time - vecCol[0].col[0].time;
    for (vector<timeCol>::size_type i = 0; i < vecCol.size(); i++) {
        if ((double) (vecCol[i].col.size()) >= prag) {
            vector<freqPt> vec = DFT(vecCol[i].col, T);
            colft.push_back(vec);
        }
    }
    vector<freqPt> combinedRes = combineMaxRes(colft);
    return combinedRes;
}

crystal mol_dyn::analysis::avgPos(const std::vector<mol_dyn::crystal> &col) {
    crystal cr;
    double size = static_cast<double>(col.size());
    cr = col[0];
    for (vector<crystal>::size_type i = 1; i < col.size(); i++) {
        for (vector<atom>::size_type j = 0; j < col[i].list.size(); j++) {
            cr.list[j].crt = math::sum(cr.list[j].crt, col[i].list[j].crt);
        }
        cr.v1 = math::sum(cr.v1, col[i].v1);
        cr.v2 = math::sum(cr.v2, col[i].v2);
        cr.v3 = math::sum(cr.v3, col[i].v3);
    }
    for (vector<atom>::size_type j = 0; j < cr.list.size(); j++) {
        cr.list[j].crt = math::mult(1.0 / size, cr.list[j].crt);
    }
    cr.v1 = math::mult(1.0 / size, cr.v1);
    cr.v2 = math::mult(1.0 / size, cr.v2);
    cr.v3 = math::mult(1.0 / size, cr.v3);
    for (int j = 0; j < cr.list.size(); j++) {
        coord dummy = math::abs2intern(cr.list[j].crt, cr);
        if (dummy.x >= 1.0) {
            dummy.x = dummy.x - 1.0;
        }
        if (dummy.x < 0.0) {
            dummy.x = dummy.x + 1.0;
        }
        if (dummy.y >= 1.0) {
            dummy.y = dummy.y - 1.0;
        }
        if (dummy.y < 0.0) {
            dummy.y = dummy.y + 1.0;
        }
        cr.list[j].crt = math::intern2abs(dummy, cr);
    }
    return cr;
}

vector<freqPt> mol_dyn::analysis::smooth(std::vector<mol_dyn::freqPt> &data, int a) {
    vector<freqPt> smData;
    for (auto i = 0; i < data.size(); i++) {
        data[i].Int = sqrt(pow(data[i].relInt, 2.0) + pow(data[i].imgInt, 2.0));
    }
    for (auto i = 0; i < data.size(); i++) {
        double aux = 0;
        int counts = 0;
        for (int j = (i - a + 1); j <= (i + a - 1); j++) {
            if (j < 0)
                continue;
            if (j >= data.size())
                continue;
            counts++;
            aux += data[j].Int * (a - abs(i - j)) / (double) a;
        }
        freqPt frAux;
        frAux.freq = data[i].freq;
        frAux.Int = aux / static_cast<double>(counts);
        smData.push_back(frAux);
    }
    return smData;
}

std::unordered_map<int, molec_positions> mol_dyn::analysis::sortByMolecule(const vector<molCol>& mCol) {
    unordered_map<int, molec_positions> ret;
    molCol firstPositions = mCol[0];

    for (auto it = firstPositions.col.cbegin(); it != firstPositions.col.cend(); ++it) {
        mol_dyn::molec_positions ins;
        ins.ID = it->ID;
        ins.positions.push_back(*it);
        ret.insert({it->ID, ins});
    }
    auto step = mCol.cbegin();
    ++step;

    for (; step != mCol.cend(); ++step) {
        for (auto i_molec = step->col.cbegin(); i_molec != step->col.cend(); ++i_molec) {
            ret[i_molec->ID].positions.push_back(*i_molec);
        }
    }
    return ret;
}

vector<double> mol_dyn::analysis::thetaDeviationsInRad(const molec_positions& mp) {
    coord initial = math::vec_orientation(mp.positions[0]);
    vector<double> ret;
    for (vector<molec>::size_type i = 1; i < mp.positions.size(); ++i) {
        coord cur_orientation = math::vec_orientation(mp.positions[i]);
        ret.push_back(math::angle(initial, cur_orientation));
    }
    return ret;
}

vector<double> mol_dyn::analysis::thetaDeviationsInRad(const molec_positions& mp, const coord& c) {
    vector<double> ret;
    for (vector<molec>::size_type i = 1; i < mp.positions.size(); ++i) {
        coord cur_orientation = math::vec_orientation(mp.positions[i]);
        double angle = math::angle(c, cur_orientation);
        ret.push_back(angle);
    }
    return ret;
}

vector<double> mol_dyn::analysis::thetaDeviationsInRad_abs(const molec_positions& mp) {
    coord initial = math::vec_orientation(mp.positions[0]);
    vector<double> ret;
    for (vector<molec>::size_type i = 1; i < mp.positions.size(); ++i) {
        coord cur_orientation = math::vec_orientation(mp.positions[i]);
        ret.push_back(std::abs(math::angle(initial, cur_orientation)));
    }
    return ret;
}
mol_dyn::molecules_snapshot mol_dyn::analysis::get_molecule_states(const molCol &mCol, const crystal& c) {
    const vector<molec>& molecules = mCol.col;
    molecules_snapshot ret;
    for(vector<molec>::const_iterator it = molecules.cbegin(); it!=molecules.cend(); ++it){
        molecule_state state;
        coord dummy;
        coord vec = analysis::perVec(it->a1.crt, it->a2.crt, c, dummy);
        state.centre_of_mass = it->a1.crt + (vec/2);
        state.sph = math::cart_to_spherical(vec);
        ret.push_back(state);
    }
    return ret;
}


Image_combined mol_dyn::analysis::boundaries_crossed(const molec &molecule, const crystal &c){

    coord atom1 = molecule.a1.crt;
    coord atom2 = molecule.a2.crt;

    coord a1int = math::abs2intern(atom1, c);
    coord a2int = math::abs2intern(atom2, c);
    Image_combined combined;
    combined.a1 = combined.a2 = combined.b1 = combined.b2 = combined.c1 = combined.c2 = NO;

    double a_x_dist = fabs(a1int.x - a2int.x);
    //image of first atom in a+1 is closer than other atom
    if (fabs((a1int.x + 1) - a2int.x) < a_x_dist){
        combined.a1 = YES;
    } //If the second one is closer to the first in a+1 in
    else if (fabs(a1int.x - (a2int.x+1)) < a_x_dist){
        combined.a2 = YES;
    }
    double a_y_dist = fabs(a1int.y - a2int.y);
    //image of first atom in a+1 is closer than other atom
    if (fabs((a1int.y+ 1) - a2int.y) < a_y_dist){
        combined.b1 = YES;
    } //If the second one is closer to the first in a+1 in
    else if (fabs(a1int.y - (a2int.y+1)) < a_y_dist){
        combined.b2 = YES;
    }
    double a_z_dist = fabs(a1int.z - a2int.z);
    //image of first atom in a+1 is closer than other atom
    if (fabs((a1int.z + 1) - a2int.z) < a_z_dist){
        combined.c1 = YES;
    } //If the second one is closer to the first in a+1 in
    else if (fabs(a1int.z - (a2int.z+1)) < a_z_dist){
        combined.c2 = YES;
    }
    return combined;
}


void mol_dyn::analysis::quadrupole_sum(const std::vector<molec>& molecules, const mol_dyn::crystal &c,
                                       double* sum_value,
                                       double nearest_neighbour_dist) {
    for(auto i = 0; i < molecules.size(); ++i){
        auto& a1 = molecules[i];
        coord com1 = math::centre_of_mass(a1, c);

        for(auto j = i + 1; j < molecules.size(); ++j) {
            auto &a2 = molecules[j];
            coord com2 = math::centre_of_mass(a2, c);
            coord dummy;

            if (perDist(com1, com2, c, dummy) < nearest_neighbour_dist) {
                (*sum_value) += math::quadrupole_interaction(a1, a2, c);
            }
        }
    }
}
