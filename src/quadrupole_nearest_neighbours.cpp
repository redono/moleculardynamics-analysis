//
// Created by s1205916 on 20/08/15.
//
#include <iostream>
#include <algorithm>
#include <sstream>
#include <fstream>
#include <thread>
#include <cmath>
#include "types.h"
#include "md_io.h"
#include "md_math.h"
#include <algorithm>
#include "md_analysis.h"

using namespace mol_dyn;
using std::ifstream;
using std::string;
using std::vector;
using std::cout;
using std::endl;
using std::cerr;
using std::stringstream;
using std::ofstream;
using std::thread;

int main(int argc, char** argv){

    if (argc != 3) {
        cerr << "Usage: "<< argv[0] << " <seedname>.md <no. atoms>" << endl;
        exit(EXIT_FAILURE);
    }
    string filename(argv[1]);
    int no_atoms = atoi(argv[2]);
    ifstream ifstr(filename);
    if (!ifstr) {
        cerr << "Could not open file. " << endl << "Error code: " << ifstr.rdstate() << endl;
        cerr << "Usage: angularvariation <seedname>.md" << endl;
        exit(EXIT_FAILURE);
    }
    vector<crystal> md = IO::loadMD(ifstr, no_atoms, 0, 0);
    analysis::identLayers(md, 6);
    math::convertUnits(md);
    vector<molCol> mCol = analysis::identMolCol(md, MolColMethod::CONST_SET);
    thread* threads = new thread[mCol.size()];
    double* values = new double[mCol.size()];
    double nearest_neighbour_cutoff = 2.4;
    for (int i = 0; i < mCol.size(); ++i) {
        values[i] = 0;
        threads[i] = std::thread(analysis::quadrupole_sum, mCol[i].col, md[i], &values[i], nearest_neighbour_cutoff);
    }
    for(int i = 0; i< mCol.size(); ++i){
        threads[i].join();
    }
    delete[] threads;

    for (int j = 0; j < mCol.size(); ++j) {
        //std::cout << j << " " << values[j] << endl;
    }
    delete[] values;
    return 0;
}
