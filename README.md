# README #
Some codes to analyse the output of Molecular Dynamics simulations done using CASTEP.

### What is this repository for? ###

Summary of the current executables:  

* angle_autocorrelation: Perform a dot product/angle autocorrelation on each of the molecules throughout the simulation. output is in the form of columns t' - average autocorrelation coefficient  
 * bond_constraints: loads up a snapshot of the simulation and determines which of the atoms form bonds crossing periodic boundaries  
* angular_variation: outputs the theta/phi spherical angles wrt the regular cartesian axes for each of the molecules over the simulation  
* centre_of_mass: Currently disfunctional
* trajectories_angles: for any snapshot in the simulation, get the centre of mass for each of the molecules and their orientation and output this to stdout
* quadrupole_nearest_neighbours: sum the quadrupole interaction energy over all molecules for all nearest neighbours. For use in comparison with potential energy etc
### How do I get set up? ###

Cmake 2.8 or higher required, cmake required, g++ 4.4.2 or higher required for C++0x/C++11 support.

run:

    $ cmake -G "Unix Makefiles"
    $ make